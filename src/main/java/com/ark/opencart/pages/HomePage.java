package com.ark.opencart.pages;

import com.microsoft.playwright.Page;

public class HomePage {

    Page page;
    //1. String Locator
    private String search = "input[name='search']";
    private String searchIcon = "div#search button";
    private String searchPageHeader = "div#content h1";
    private String loginlink = "a:text('Login')";
    private String myAaccountLink = "a[title='My Account']";

    //2. Page Constructor

    public HomePage(Page page) {
        this.page = page;
    }

    //3. Methods for page actions

    public String getHomePageTitle(){
        String title = page.title();
        System.out.println("Page Title : "+title);
        return page.title();
    }

    public String getHomePageURL(){
        String url = page.url();
        System.out.println("Page URL : "+url);
        return page.url();
    }

    public String doSearch(String productName){
        page.fill(search,productName);
        page.click(searchIcon);
        String header = page.textContent(searchPageHeader);
        System.out.println("Header value : "+header);
        return page.textContent(searchPageHeader);
    }

    public LoginPage navigateToLoginPage(){
        page.click(myAaccountLink);
        page.click(loginlink);
        return new LoginPage(page);
    }
}


