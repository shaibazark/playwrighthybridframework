package com.ark.opencart.pages;

import com.microsoft.playwright.Page;

public class LoginPage {

    Page page;
    //1. String Locator
    private String emailIdField = "//input[@id='input-email']";
    private String passwordField = "//input[@id='input-password']";
    private String loginButton = "//input[@value='Login']";
    private String forgotPasswordLink = "//div[@class='form-group']//a[normalize-space()='Forgotten Password']";
    private String logoutLink = "//a[@class='list-group-item'][normalize-space()='Logout']";

    //2. Constructor of page class
    public LoginPage(Page page){
        this.page = page;
    }

    public String loginPageTitle(){
        return page.title();
    }

    public boolean isForgotPasswordLinkExist(){
        return page.isVisible(forgotPasswordLink);
    }

    public boolean doLogin(String uname, String password){
        page.fill(emailIdField,uname);
        page.fill(passwordField,password);
        page.click(loginButton);
        if(page.isVisible(logoutLink)){
            System.out.println("User is logged in successfully . . . ");
            return true;
        }
        return false;
    }


}
