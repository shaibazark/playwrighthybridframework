package com.ark.opencart.tests;

import com.ark.opencart.base.BaseTestClass;
import com.ark.opencart.constants.Constants;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPageTest extends BaseTestClass {

    @Test(priority = 0)
    public void loginPageNavigationTest(){
        loginPage = homePage.navigateToLoginPage();
        String actualTitle = loginPage.loginPageTitle();
        System.out.println(actualTitle);
        Assert.assertEquals(actualTitle, Constants.LOGIN_PAGE_TITLE);
    }

    @Test(priority = 1)
    public void forgotPasswordLinkTest(){
        Assert.assertTrue(loginPage.isForgotPasswordLinkExist());
    }

    @Test(priority = 2)
    public void LoginTest(){
        loginPage.doLogin(properties.getProperty("username"),properties.getProperty("password"));
    }

}
