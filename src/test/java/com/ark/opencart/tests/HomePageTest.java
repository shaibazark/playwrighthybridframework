package com.ark.opencart.tests;

import com.ark.opencart.base.BaseTestClass;
import com.ark.opencart.constants.Constants;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HomePageTest extends BaseTestClass {


    @Test(priority = 0)
    public void homePageTitleTest(){
        String actualTitle = homePage.getHomePageTitle();
        Assert.assertEquals(actualTitle, Constants.HOME_PAGE_TITLE);
    }

    @Test(priority = 1)
    public void homePageURLTest(){
        String actualURL = homePage.getHomePageURL();
        Assert.assertEquals(actualURL,properties.getProperty("url"));
    }

    @DataProvider
    public Object[][] getProductData(){
        return new Object[][] {
                {"Macbook"},
                {"iMack"},
                {"Samsung"}
        };
    }

    @Test(priority = 2 , dataProvider = "getProductData")
    public void homePageSearchTest(String productName){
        String actualContent = homePage.doSearch(productName);
        Assert.assertEquals(actualContent,"Search - "+productName);
    }

}
