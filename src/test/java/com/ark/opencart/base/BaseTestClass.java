package com.ark.opencart.base;

import com.ark.opencart.PlaywrightFactory;
import com.ark.opencart.pages.HomePage;
import com.ark.opencart.pages.LoginPage;
import com.microsoft.playwright.Page;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.Properties;

public class BaseTestClass {


    Page page;
    public PlaywrightFactory playwrightFactory;
    public HomePage homePage;
    public LoginPage loginPage;
    protected Properties properties;
    @BeforeTest
    public void setup(){
        playwrightFactory = new PlaywrightFactory();
        properties = playwrightFactory.init_prop();
        page = playwrightFactory.initBrowser(properties);
        homePage = new HomePage(page);
    }


    @AfterTest
    public void tearDown(){
        page.context().browser().close();
    }
}
